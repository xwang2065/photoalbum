﻿/***
 * Student Name: Xiaoyang Wang
 * Create Date: 
 * Discription: Photo Class. 
 *              This class for managing all aspects of the photo,
 *              including photo path and photo description.
 */
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoAlbum
{
    class Photo
    {
        string photoPath;
        string photoDescription;

        const char DELIMETER = '|';

        public Photo(string photoPath)
        {
            this.PhotoPath = photoPath;
        }

        public Photo()
        {

        }

        public string PhotoPath { get => photoPath; set => photoPath = value; }
        public string PhotoDescription { get => photoDescription; set => photoDescription = value; }

        /// <summary>
        /// This is a method for changing the photo info into string form, which is ready to store in file.
        /// </summary>
        public override string ToString()
        {
            return $"{PhotoPath}{DELIMETER}{photoDescription}";
        }

        public string GetPhotoCreatedTimeInfo()
        {
            string timeInfo = $"File Created: {File.GetCreationTime(PhotoPath).ToString("g")}";
            return timeInfo;
        }

        /// <summary>
        /// This is a method to split the fileLine into an string array and return the array.
        /// </summary>
        public string[] SplitFileLine(string line)
        {
            string[] splitedInfo = line.Split(DELIMETER);

            return splitedInfo;
        }
    }
}
