﻿/***
 * Student Name: Xiaoyang Wang
 * Create Date: 
 * Discription: Create New Album Form. 
 *              This is a secondary form for creating the name of new album,
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhotoAlbum
{
    public partial class CreateNewAlbumForm : Form
    {
        string albumName;

        public CreateNewAlbumForm()
        {
            InitializeComponent();
        }

        public string AlbumName { get => albumName; set => albumName = value; }

        /// <summary>
        /// This is an event handler method for btnCreate,
        /// to let the main form get the name created in the textbox if the name input is not empty,
        /// and to close this form after creating successfully
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnCreate_Click(object sender, EventArgs e)
        {
            if (txtAlbumName.Text.Trim() != "")
            {
                PhotoAlbumForm photoAlbumForm = (PhotoAlbumForm)Owner;
                albumName = txtAlbumName.Text;
                photoAlbumForm.EnableNeededControls();
                this.DialogResult = DialogResult.OK;
            }
            else
            {
                MessageBox.Show("Album name can not be empty, please enter again.", "Error",
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// This is an event handler method for btnCancel,
        /// to cancel the creating and close this form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
