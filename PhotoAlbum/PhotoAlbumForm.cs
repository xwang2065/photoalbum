﻿/***
 * Student Name: Xiaoyang Wang
 * Create Date: 
 * Discription: Assignment 2. 
 *              This is a comprehensive exercise for the study in past few weeks, 
 *              which help us to practice IO and Windows Forms especially the use of
 *              Save/Open File Dialog, and to familiar with creating class and object.
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhotoAlbum
{
    public partial class PhotoAlbumForm : Form
    {
        PhotoAlbum photoAlbum = new PhotoAlbum();

        /*the initial folder is seted to the Pictures folder in the computer 
        /*cause everything should be related to pictures
         */
        string initialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures).ToString();
    
        bool isAlbumSaved = false;

        public PhotoAlbumForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// This is an event handler method for PhotoAlbumForm when it is loading,
        /// to set the canvas default back colour as black
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PhotoAlbumForm_Load(object sender, EventArgs e)
        {
            picDisplayPhoto.BackColor = Color.Black;
        }

        /// <summary>
        /// This is an event handler method for radBlack,
        /// to set the canvas back colour as black when the radio button is checked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RadBlack_CheckedChanged(object sender, EventArgs e)
        {
            picDisplayPhoto.BackColor = Color.Black;
        }

        /// <summary>
        /// This is an event handler method for radWhite,
        /// to set the canvas back colour as white when the radio button is checked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RadWhite_CheckedChanged(object sender, EventArgs e)
        {
            picDisplayPhoto.BackColor = Color.White;
        }

        /// <summary>
        /// This is an event handler method for radGrey,
        /// to set the canvas back colour as grey when the radio button is checked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RadGrey_CheckedChanged(object sender, EventArgs e)
        {
            picDisplayPhoto.BackColor = Color.Gray;
        }

        /// <summary>
        /// This is an event handler method for btnCreateAlbum,
        /// to create the name of the new photo album, 
        /// and when there was an open album, to prompt user if they need to save it 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnCreateAlbum_Click(object sender, EventArgs e)
        {
            //to prompt user to save the open album if there is one exist
            if (photoAlbum.AlbumName != null)
            {
                DialogResult result = MessageBox.Show("Do you want to save the album before creating a new album?",
                                                      "Closing", MessageBoxButtons.YesNoCancel);
                /*if user want to save the open album, save, reset the form, and do next step
                 * if user don't want to save, skip the save part.
                 */
                if (result == DialogResult.Yes)
                {
                    SaveAlbum();

                    /*In the save dialog, if user click on cancel or the X in the top right corner of the dialog,
                     * then the saving is not success.
                     * This will give user another opportunity to save in case of misoprration
                     */
                    if (isAlbumSaved)
                    {
                        ResetPhotoAlbumForm();
                        CreateNewAlbum();
                    }
                    else
                    {
                        DialogResult rslt = MessageBox.Show("Saving is not successful, continue?",
                                                            "Save Failed", MessageBoxButtons.YesNo);
                        if (rslt == DialogResult.Yes)
                        {
                            ResetPhotoAlbumForm();
                            CreateNewAlbum();
                        }
                    }
                }
                if (result == DialogResult.No)
                {
                    ResetPhotoAlbumForm();
                    CreateNewAlbum();
                }
            }
            else
            {
                CreateNewAlbum();
            }

            //give a focus on the add photo button
            btnAddPhotos.Focus();
        }

        /// <summary>
        /// This is method for opening CreateNewAlbumForm to create the name of the album,
        /// and show the created name in the tile bar of the main form
        /// </summary>
        private void CreateNewAlbum()
        {
            CreateNewAlbumForm createNewAlbumForm = new CreateNewAlbumForm();
            if (createNewAlbumForm.ShowDialog(this) == DialogResult.OK)
            {
                photoAlbum.AlbumName = createNewAlbumForm.AlbumName;
                this.Text = $"Photo Album - {photoAlbum.AlbumName}";
            }
        }

        /// <summary>
        /// This is method for enabling the needded controls after creating a new album
        /// </summary>
        public void EnableNeededControls()
        {
            pnlInformationAndNavigation.Enabled = true;
            txtDescription.Enabled = false;
            btnPrevious.Enabled = false;
            btnNext.Enabled = false;
            btnSaveAlbum.Enabled = true;
        }

        /// <summary>
        /// This is an event handler method for btnAddPhotos,
        /// for adding photos to album using OpenFileDialog
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnAddPhotos_Click(object sender, EventArgs e)
        {
            openFileDialog.InitialDirectory = initialDirectory;
            openFileDialog.Filter = "Image Files (*.jpeg;*.jpg)|*.jpeg;*.jpg";
            openFileDialog.Multiselect = true;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                btnPrevious.Enabled = true;
                btnNext.Enabled = true;
                txtDescription.Enabled = true;
                lblPhotoCreatedTimeInfo.Visible = true;

                string[] addingPhotoPaths = openFileDialog.FileNames;
                int orignalListCount = photoAlbum.PhotoList.Count;
                int firstShownPhotoIndex = photoAlbum.PhotoList.Count;

                for (int i = 0; i < addingPhotoPaths.Length; i++)
                {
                    /*If some photos already in the open album, 
                    /*should check if the adding photo(s) already exist in this album
                     */
                    if (photoAlbum.PhotoList.Count != 0)
                    {
                        if (!photoAlbum.PhotoList.Exists(photo => photo.PhotoPath == addingPhotoPaths[i]))
                        {
                            Photo photo = new Photo(addingPhotoPaths[i]);
                            photoAlbum.PhotoList.Add(photo);
                        }
                    }
                    else
                    {
                        Photo photo = new Photo(addingPhotoPaths[i]);
                        photoAlbum.PhotoList.Add(photo);
                    }
                }

                /*If the adding photos are all existed in the album, no photos are added in this event 
                 * and the displayed photo keep being the one displayed before the event.
                 * If some of the adding photos are existed in the album, prompt user some photos not added 
                 * and diaplay the first successful adding photo.
                 * Otherwise, display the first adding photo.
                 */
                if (photoAlbum.PhotoList.Count == orignalListCount)
                {
                    MessageBox.Show("Photo(s) already existed and were not added.");
                    Photo currentShowingPhoto = photoAlbum.PhotoList.Find(photo => photo.PhotoPath == picDisplayPhoto.ImageLocation);
                    int currentShowingPhotoIndex = photoAlbum.PhotoList.IndexOf(currentShowingPhoto);
                    firstShownPhotoIndex = currentShowingPhotoIndex;
                }
                else
                {
                    if (photoAlbum.PhotoList.Count < orignalListCount + addingPhotoPaths.Length)
                    {
                        int photoNotAddedNo;
                        photoNotAddedNo = orignalListCount + addingPhotoPaths.Length - photoAlbum.PhotoList.Count;
                        MessageBox.Show($"{photoNotAddedNo} photo(s) already existed and were not added.");
                    }

                    picDisplayPhoto.ImageLocation = photoAlbum.PhotoList[firstShownPhotoIndex].PhotoPath;
                    lblPhotoCreatedTimeInfo.Text = photoAlbum.PhotoList[firstShownPhotoIndex].GetPhotoCreatedTimeInfo();
                    txtDescription.Text = photoAlbum.PhotoList[firstShownPhotoIndex].PhotoDescription;
                }                  
            }

            //give description textbox focus to make user easy to enter the description
            txtDescription.Focus();
        }

        /// <summary>
        /// This is an event handler method for btnPrevious,
        /// for showing the previous photo in the album, 
        /// and the last photo of the album if present one is the first one of the album.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnPrevious_Click(object sender, EventArgs e)
        {
            int previousPhotoIndex = photoAlbum.GetPreviousPhotoIndex(picDisplayPhoto.ImageLocation);

            picDisplayPhoto.ImageLocation = photoAlbum.PhotoList[previousPhotoIndex].PhotoPath;
            lblPhotoCreatedTimeInfo.Text = photoAlbum.PhotoList[previousPhotoIndex].GetPhotoCreatedTimeInfo();

            if (photoAlbum.PhotoList[previousPhotoIndex].PhotoDescription == null)
            {
                txtDescription.Clear();
            }
            else
            {
                txtDescription.Text = photoAlbum.PhotoList[previousPhotoIndex].PhotoDescription;
            }

            //give description textbox focus to make user easy to enter the description
            txtDescription.Focus();
        }

        /// <summary>
        /// This is an event handler method for btnNext,
        /// for showing the next photo in the album, 
        /// and the first photo of the album if present one is the last one of the album.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnNext_Click(object sender, EventArgs e)
        {
            int nextPhotoIndex = photoAlbum.GetNextPhotoIndex(picDisplayPhoto.ImageLocation);

            picDisplayPhoto.ImageLocation = photoAlbum.PhotoList[nextPhotoIndex].PhotoPath;
            lblPhotoCreatedTimeInfo.Text = photoAlbum.PhotoList[nextPhotoIndex].GetPhotoCreatedTimeInfo();

            if (photoAlbum.PhotoList[nextPhotoIndex].PhotoDescription == null)
            {
                txtDescription.Clear();
            }
            else
            {
                txtDescription.Text = photoAlbum.PhotoList[nextPhotoIndex].PhotoDescription;
            }

            //give description textbox focus to make user easy to enter the description
            txtDescription.Focus();
        }

        /// <summary>
        /// This is an event handler method for txtDescription when leaving the text box,
        /// for saving the description of the shown phot
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TxtDescription_Leave(object sender, EventArgs e)
        {
            Photo photoShown = photoAlbum.PhotoList.Find(photo => photo.PhotoPath == picDisplayPhoto.ImageLocation);
            photoShown.PhotoDescription = txtDescription.Text;
        }

        /// <summary>
        /// This is an event handler method for BtnSaveAlbum,
        /// for saving the album and appending the Album Path to the title bar of the main form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnSaveAlbum_Click(object sender, EventArgs e)
        {
            SaveAlbum();
            this.Text = $"Photo Album - {photoAlbum.AlbumName} - {photoAlbum.AlbumPath}";
        }

        /// <summary>
        /// This is method for saving the album to file and show the user album saved.
        /// If there is already a path for the album, save the file there directly.
        /// If not, use SaveFileDialog to save the album.
        /// </summary>
        private void SaveAlbum()
        {
            if (photoAlbum.AlbumPath != null)
            {
                photoAlbum.SavePhotoAlbumToFile();

                MessageBox.Show($"Album {photoAlbum.AlbumName} saved!", "Saved");
                isAlbumSaved = true;
            }
            else
            {
                saveFileDialog.InitialDirectory = initialDirectory;
                saveFileDialog.FileName = photoAlbum.AlbumName;
                saveFileDialog.Filter = "Album(*.alb)|*.alb";

                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    photoAlbum.AlbumPath = saveFileDialog.FileName;
                    photoAlbum.AlbumName = Path.GetFileNameWithoutExtension(photoAlbum.AlbumPath);
                    photoAlbum.SavePhotoAlbumToFile();

                    DialogResult rslt = MessageBox.Show($"Album {photoAlbum.AlbumName} saved!", "Saved");
                    isAlbumSaved = true;
                }
                else
                {
                    isAlbumSaved = false;
                }
            }
        }

        /// <summary>
        /// This is an event handler method for btnOpenAlbu,
        /// to open an album using OpenFileDialog, 
        /// and when there was an open album, to prompt user if they need to save it 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnOpenAlbum_Click(object sender, EventArgs e)
        {
            //to prompt user to save the open album if there is one exist
            if (photoAlbum.AlbumName != null)
            {
                DialogResult result = MessageBox.Show("Do you want to save the album before opening another album?",
                                                      "Closing", MessageBoxButtons.YesNoCancel);

                /*if user want to save the open album, save, reset the form, and do next step
                 * if user don't want to save, skip the save part.
                 */
                if (result == DialogResult.Yes)
                {
                    SaveAlbum();

                    /*In the save dialog, if user click on cancel or the X in the top right corner of the dialog,
                     * then the saving is not success.
                     * This will give user another opportunity to save in case of misoprration
                     */
                    if (isAlbumSaved)
                    {
                        ResetPhotoAlbumForm();
                        OpenAlbum();
                    }
                    else
                    {
                        DialogResult rslt = MessageBox.Show("Saving is not successful, continue?",
                                                            "Save Failed", MessageBoxButtons.YesNo);
                        if (rslt == DialogResult.Yes)
                        {
                            ResetPhotoAlbumForm();
                            OpenAlbum();
                        }
                    }
                }
                if (result == DialogResult.No)
                {
                    ResetPhotoAlbumForm();
                    OpenAlbum();
                }
            }
            else
            {
                OpenAlbum();
            }


            //give a focus on the add photo button
            btnAddPhotos.Focus();
        }

        /// <summary>
        /// This is method for opening the album using OpenFileDialog.
        /// Read the chosen file and add the info to the photo list of album, then show the photos in the list
        /// </summary>
        private void OpenAlbum()
        {
            openFileDialog.InitialDirectory = initialDirectory;
            openFileDialog.Filter = "Album(*.alb)|*.alb";

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                photoAlbum.AlbumPath = openFileDialog.FileName;

                int albumFileLinesCount = photoAlbum.ReadPhotoAlbumFromFile();

                this.Text = $"Photo Album - {photoAlbum.AlbumName} - {photoAlbum.AlbumPath}";

                pnlInformationAndNavigation.Enabled = true;
                btnSaveAlbum.Enabled = true;
                lblPhotoCreatedTimeInfo.Visible = true;

                /*If only one line in the file, which means there is only album name saved in the fiel,
                 * and this album is empty.
                 */
                if (albumFileLinesCount == 1)
                {
                    btnNext.Enabled = false;
                    btnPrevious.Enabled = false;
                    txtDescription.Enabled = false;
                }
                else
                {
                    btnNext.Enabled = true;
                    btnPrevious.Enabled = true;
                    txtDescription.Enabled = true;

                    picDisplayPhoto.ImageLocation = photoAlbum.PhotoList[0].PhotoPath;
                    txtDescription.Text = photoAlbum.PhotoList[0].PhotoDescription;
                    lblPhotoCreatedTimeInfo.Text = photoAlbum.PhotoList[0].GetPhotoCreatedTimeInfo();
                }
            }
        }

        /// <summary>
        /// This is an event handler method for PhotoAlbumForm when it is closing,
        /// to allow th user to decide if need to save the open album or not when there is an open one.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PhotoAlbumForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (photoAlbum.AlbumName != null)
            {
                DialogResult result = MessageBox.Show("Do you want to save the album before closing?",
                                                      "Closing", MessageBoxButtons.YesNoCancel);
                /*if user want to save the open album, save and exit the application
                 * if user don't want to save, exit the application.
                 */
                if (result == DialogResult.Yes)
                {
                    /*If there is a photo shown in the picture box, user probablly changed the description.
                     * And if user click X to exit the application right after the changing, 
                     * the leaving event handler of txtDescription will not work.
                     * So need to save the description of the photo first befor saving the album.
                     */
                    if (picDisplayPhoto.ImageLocation != null)
                    {
                        Photo photoShown = photoAlbum.PhotoList.Find(photo => photo.PhotoPath == picDisplayPhoto.ImageLocation);
                        photoShown.PhotoDescription = txtDescription.Text;
                    }
                    SaveAlbum();

                    /*In the save dialog, if user click on cancel or the X in the top right corner of the dialog,
                     * then the saving is not success.
                     * This will give user another opportunity to save in case of misoprration
                     */
                    if (isAlbumSaved)
                    {
                        photoAlbum.AlbumName = null;
                        Application.Exit();
                    }
                    else
                    {
                        DialogResult rslt = MessageBox.Show("Saving is not successful, continue?",
                                                            "Save Failed", MessageBoxButtons.YesNo);
                        if (rslt == DialogResult.Yes)
                        {
                            photoAlbum.AlbumName = null;
                            Application.Exit();
                        }
                        else
                        {
                            //Cancel the closing event if user want to save the file before closing
                            e.Cancel = true;
                        }
                    }
                }
                if (result == DialogResult.No)
                {
                    photoAlbum.AlbumName = null;
                    Application.Exit();
                }
                else
                {
                    //Cancel the closing event if user click Cancel
                    e.Cancel = true;
                }
            }
            else
            {
                Application.Exit();
            }
        }

        /// <summary>
        /// This is a method for reset the main form and clear the photoList of photoAlbum
        /// Get ready for next event.
        /// </summary>
        private void ResetPhotoAlbumForm()
        {
            picDisplayPhoto.ImageLocation = null;
            txtDescription.Clear();
            lblPhotoCreatedTimeInfo.Visible = false;
            pnlInformationAndNavigation.Enabled = false;
            btnSaveAlbum.Enabled = false;

            photoAlbum.PhotoList.Clear();
            photoAlbum.AlbumName = null;
            photoAlbum.AlbumPath = null;

            this.Text = "Photo Album";
        }
    }
}
