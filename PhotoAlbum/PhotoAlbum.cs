﻿/***
 * Student Name: Xiaoyang Wang
 * Create Date: 
 * Discription: PhotoAlbum Class. 
 *              This class for managing all aspects of the photo album,
 *              including album name, album path and photo list in the album.
 */
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoAlbum
{
    class PhotoAlbum
    {
        string albumName;
        string albumPath;
        List<Photo> photoList = new List<Photo>();

        public string AlbumName { get => albumName; set => albumName = value; }
        public string AlbumPath { get => albumPath; set => albumPath = value; }
        internal List<Photo> PhotoList { get => photoList; set => photoList = value; }

        /// <summary>
        /// This is a method for saving the album to file.
        /// </summary>
        public void SavePhotoAlbumToFile()
        {
            using (StreamWriter writer = File.CreateText(albumPath))
            {
                writer.WriteLine(albumName);
                foreach (Photo photo in photoList)
                {
                    writer.WriteLine(photo.ToString());
                }
            }
        }

        /// <summary>
        /// This is a method for reading back the album from file.
        /// </summary>
        public int ReadPhotoAlbumFromFile()
        {
            string[] fileLines = File.ReadAllLines(albumPath);

            albumName = fileLines[0];

            for (int i = 1; i < fileLines.Length; i++)
            {
                Photo photoInFile = new Photo();

                string[] splitedPhotoInfo = photoInFile.SplitFileLine(fileLines[i]);
                photoInFile.PhotoPath = splitedPhotoInfo[0];
                photoInFile.PhotoDescription = splitedPhotoInfo[1];

                photoList.Add(photoInFile);
            }

            return fileLines.Length;
        }

        /// <summary>
        /// This is a method for returning the index of previous photo.
        /// </summary>
        public int GetPreviousPhotoIndex(string path)
        {
            Photo photoShown = photoList.Find(currentPhoto => currentPhoto.PhotoPath == path);
            int currentIndex = photoList.IndexOf(photoShown);

            int previousIndex;

            if (currentIndex == 0)
            {
                previousIndex = photoList.Count - 1;

            }
            else
            {
                previousIndex = currentIndex - 1;
            }

            return previousIndex;
        }

        /// <summary>
        /// This is a method for returning the index of next photo.
        /// </summary>
        public int GetNextPhotoIndex(string path)
        {
            Photo photoShown = photoList.Find(photo => photo.PhotoPath == path);
            int presentIndex = photoList.IndexOf(photoShown);

            int nextIndex;

            if (presentIndex == photoList.Count - 1)
            {
                nextIndex = 0;
            }
            else
            {
                nextIndex = presentIndex + 1;
            }

            return nextIndex;
        }
    }
}
