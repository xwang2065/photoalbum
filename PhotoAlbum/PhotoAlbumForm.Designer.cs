﻿namespace PhotoAlbum
{
    partial class PhotoAlbumForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.picDisplayPhoto = new System.Windows.Forms.PictureBox();
            this.grbCanvasColour = new System.Windows.Forms.GroupBox();
            this.radGrey = new System.Windows.Forms.RadioButton();
            this.radWhite = new System.Windows.Forms.RadioButton();
            this.radBlack = new System.Windows.Forms.RadioButton();
            this.pnlInformationAndNavigation = new System.Windows.Forms.Panel();
            this.lblPhotoCreatedTimeInfo = new System.Windows.Forms.Label();
            this.btnNext = new System.Windows.Forms.Button();
            this.btnPrevious = new System.Windows.Forms.Button();
            this.btnAddPhotos = new System.Windows.Forms.Button();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.lblDescription = new System.Windows.Forms.Label();
            this.btnCreateAlbum = new System.Windows.Forms.Button();
            this.btnOpenAlbum = new System.Windows.Forms.Button();
            this.btnSaveAlbum = new System.Windows.Forms.Button();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.picDisplayPhoto)).BeginInit();
            this.grbCanvasColour.SuspendLayout();
            this.pnlInformationAndNavigation.SuspendLayout();
            this.SuspendLayout();
            // 
            // picDisplayPhoto
            // 
            this.picDisplayPhoto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picDisplayPhoto.Location = new System.Drawing.Point(50, 50);
            this.picDisplayPhoto.Name = "picDisplayPhoto";
            this.picDisplayPhoto.Size = new System.Drawing.Size(547, 451);
            this.picDisplayPhoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picDisplayPhoto.TabIndex = 0;
            this.picDisplayPhoto.TabStop = false;
            // 
            // grbCanvasColour
            // 
            this.grbCanvasColour.Controls.Add(this.radGrey);
            this.grbCanvasColour.Controls.Add(this.radWhite);
            this.grbCanvasColour.Controls.Add(this.radBlack);
            this.grbCanvasColour.Location = new System.Drawing.Point(623, 338);
            this.grbCanvasColour.Name = "grbCanvasColour";
            this.grbCanvasColour.Size = new System.Drawing.Size(381, 100);
            this.grbCanvasColour.TabIndex = 10;
            this.grbCanvasColour.TabStop = false;
            this.grbCanvasColour.Text = "Canvas Colour";
            // 
            // radGrey
            // 
            this.radGrey.AutoSize = true;
            this.radGrey.Location = new System.Drawing.Point(301, 40);
            this.radGrey.Name = "radGrey";
            this.radGrey.Size = new System.Drawing.Size(68, 24);
            this.radGrey.TabIndex = 3;
            this.radGrey.Text = "Grey";
            this.radGrey.UseVisualStyleBackColor = true;
            this.radGrey.CheckedChanged += new System.EventHandler(this.RadGrey_CheckedChanged);
            // 
            // radWhite
            // 
            this.radWhite.AutoSize = true;
            this.radWhite.Location = new System.Drawing.Point(162, 40);
            this.radWhite.Name = "radWhite";
            this.radWhite.Size = new System.Drawing.Size(75, 24);
            this.radWhite.TabIndex = 9;
            this.radWhite.Text = "White";
            this.radWhite.UseVisualStyleBackColor = true;
            this.radWhite.CheckedChanged += new System.EventHandler(this.RadWhite_CheckedChanged);
            // 
            // radBlack
            // 
            this.radBlack.AutoSize = true;
            this.radBlack.Checked = true;
            this.radBlack.Location = new System.Drawing.Point(25, 40);
            this.radBlack.Name = "radBlack";
            this.radBlack.Size = new System.Drawing.Size(73, 24);
            this.radBlack.TabIndex = 8;
            this.radBlack.TabStop = true;
            this.radBlack.Text = "Black";
            this.radBlack.UseVisualStyleBackColor = true;
            this.radBlack.CheckedChanged += new System.EventHandler(this.RadBlack_CheckedChanged);
            // 
            // pnlInformationAndNavigation
            // 
            this.pnlInformationAndNavigation.Controls.Add(this.lblPhotoCreatedTimeInfo);
            this.pnlInformationAndNavigation.Controls.Add(this.btnNext);
            this.pnlInformationAndNavigation.Controls.Add(this.btnPrevious);
            this.pnlInformationAndNavigation.Controls.Add(this.btnAddPhotos);
            this.pnlInformationAndNavigation.Controls.Add(this.txtDescription);
            this.pnlInformationAndNavigation.Controls.Add(this.lblDescription);
            this.pnlInformationAndNavigation.Enabled = false;
            this.pnlInformationAndNavigation.Location = new System.Drawing.Point(623, 50);
            this.pnlInformationAndNavigation.Name = "pnlInformationAndNavigation";
            this.pnlInformationAndNavigation.Size = new System.Drawing.Size(375, 256);
            this.pnlInformationAndNavigation.TabIndex = 4;
            // 
            // lblPhotoCreatedTimeInfo
            // 
            this.lblPhotoCreatedTimeInfo.AutoSize = true;
            this.lblPhotoCreatedTimeInfo.Location = new System.Drawing.Point(3, 175);
            this.lblPhotoCreatedTimeInfo.Name = "lblPhotoCreatedTimeInfo";
            this.lblPhotoCreatedTimeInfo.Size = new System.Drawing.Size(0, 20);
            this.lblPhotoCreatedTimeInfo.TabIndex = 11;
            this.lblPhotoCreatedTimeInfo.Visible = false;
            // 
            // btnNext
            // 
            this.btnNext.Location = new System.Drawing.Point(250, 216);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(120, 37);
            this.btnNext.TabIndex = 4;
            this.btnNext.Text = "Next";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.BtnNext_Click);
            // 
            // btnPrevious
            // 
            this.btnPrevious.Location = new System.Drawing.Point(125, 216);
            this.btnPrevious.Name = "btnPrevious";
            this.btnPrevious.Size = new System.Drawing.Size(120, 37);
            this.btnPrevious.TabIndex = 3;
            this.btnPrevious.Text = "Previous";
            this.btnPrevious.UseVisualStyleBackColor = true;
            this.btnPrevious.Click += new System.EventHandler(this.BtnPrevious_Click);
            // 
            // btnAddPhotos
            // 
            this.btnAddPhotos.Location = new System.Drawing.Point(0, 216);
            this.btnAddPhotos.Name = "btnAddPhotos";
            this.btnAddPhotos.Size = new System.Drawing.Size(120, 37);
            this.btnAddPhotos.TabIndex = 1;
            this.btnAddPhotos.Text = "Add Photos";
            this.btnAddPhotos.UseVisualStyleBackColor = true;
            this.btnAddPhotos.Click += new System.EventHandler(this.BtnAddPhotos_Click);
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(0, 23);
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(370, 137);
            this.txtDescription.TabIndex = 2;
            this.txtDescription.Leave += new System.EventHandler(this.TxtDescription_Leave);
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Location = new System.Drawing.Point(-4, 0);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(93, 20);
            this.lblDescription.TabIndex = 0;
            this.lblDescription.Text = "Description:";
            // 
            // btnCreateAlbum
            // 
            this.btnCreateAlbum.Location = new System.Drawing.Point(623, 464);
            this.btnCreateAlbum.Name = "btnCreateAlbum";
            this.btnCreateAlbum.Size = new System.Drawing.Size(120, 37);
            this.btnCreateAlbum.TabIndex = 5;
            this.btnCreateAlbum.Text = "Create Album";
            this.btnCreateAlbum.UseVisualStyleBackColor = true;
            this.btnCreateAlbum.Click += new System.EventHandler(this.BtnCreateAlbum_Click);
            // 
            // btnOpenAlbum
            // 
            this.btnOpenAlbum.Location = new System.Drawing.Point(748, 464);
            this.btnOpenAlbum.Name = "btnOpenAlbum";
            this.btnOpenAlbum.Size = new System.Drawing.Size(120, 37);
            this.btnOpenAlbum.TabIndex = 6;
            this.btnOpenAlbum.Text = "Open Album";
            this.btnOpenAlbum.UseVisualStyleBackColor = true;
            this.btnOpenAlbum.Click += new System.EventHandler(this.BtnOpenAlbum_Click);
            // 
            // btnSaveAlbum
            // 
            this.btnSaveAlbum.Enabled = false;
            this.btnSaveAlbum.Location = new System.Drawing.Point(873, 464);
            this.btnSaveAlbum.Name = "btnSaveAlbum";
            this.btnSaveAlbum.Size = new System.Drawing.Size(120, 37);
            this.btnSaveAlbum.TabIndex = 7;
            this.btnSaveAlbum.Text = "Save Album";
            this.btnSaveAlbum.UseVisualStyleBackColor = true;
            this.btnSaveAlbum.Click += new System.EventHandler(this.BtnSaveAlbum_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog";
            // 
            // PhotoAlbumForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1043, 547);
            this.Controls.Add(this.btnSaveAlbum);
            this.Controls.Add(this.btnOpenAlbum);
            this.Controls.Add(this.btnCreateAlbum);
            this.Controls.Add(this.pnlInformationAndNavigation);
            this.Controls.Add(this.grbCanvasColour);
            this.Controls.Add(this.picDisplayPhoto);
            this.MaximizeBox = false;
            this.Name = "PhotoAlbumForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Photo Album";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PhotoAlbumForm_FormClosing);
            this.Load += new System.EventHandler(this.PhotoAlbumForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picDisplayPhoto)).EndInit();
            this.grbCanvasColour.ResumeLayout(false);
            this.grbCanvasColour.PerformLayout();
            this.pnlInformationAndNavigation.ResumeLayout(false);
            this.pnlInformationAndNavigation.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox picDisplayPhoto;
        private System.Windows.Forms.GroupBox grbCanvasColour;
        private System.Windows.Forms.RadioButton radGrey;
        private System.Windows.Forms.RadioButton radWhite;
        private System.Windows.Forms.RadioButton radBlack;
        private System.Windows.Forms.Panel pnlInformationAndNavigation;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Button btnPrevious;
        private System.Windows.Forms.Button btnAddPhotos;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.Button btnCreateAlbum;
        private System.Windows.Forms.Button btnOpenAlbum;
        private System.Windows.Forms.Button btnSaveAlbum;
        private System.Windows.Forms.Label lblPhotoCreatedTimeInfo;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
    }
}

