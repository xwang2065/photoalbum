## Download the app:

1. Click the **...** button under the **Source** heading.
2. Click the **Download repository** button.
3. Select your local directory where you want to download this app.
4. The zip file will be downloaded into this directory.

## Build the app (Environment setup):

1. Install the version 4.8 of .NET Framework. It is preinstalled on Windows 10. 
   The installer also can be find from [here](https://docs.microsoft.com/en-us/dotnet/framework/install/guide-for-developers).
2. Select the language or languages supported by the .NET Framework version. Here, [C#](https://docs.microsoft.com/en-us/dotnet/csharp/) is recommeneded.
3. Select and install the development environment to use to build the apps. The Microsoft integrated development environment (IDE) for .NET Framework apps is [Visual Studio](https://visualstudio.microsoft.com/vs/?utm_medium=microsoft&utm_source=docs.microsoft.com&utm_campaign=inline+link).
4. Now, the app can be openned and built using Visual Studio.

## Build and run the app on your machine:

1. In Visual Studio, click Build > Build Solution. The Output window shows the results of the build process.
2. To run the code, on the menu bar, choose Debug > Start without debugging.

## License:

This app is using the BSD license. Becasue this app should place minimal restrictions on future behavior. 
Developers should be couraged to spend their time creating and promoting excellent code 
instead of worrying license violating.